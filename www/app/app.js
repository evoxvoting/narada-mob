// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'ngCordova'])

    .run(function ($ionicPlatform, $rootScope) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            var notificationOpenedCallback = function(jsonData) {
                console.log('Push message received');
                $rootScope.$broadcast('push-message', jsonData);
            };

            window.plugins.OneSignal.init("30c7a2bb-74fc-410d-b622-ac76dbffa0e8",
                {googleProjectNumber: "857725894177"},
                notificationOpenedCallback);

            // Show an alert box if a notification comes in when the user is in your app.
            window.plugins.OneSignal.enableInAppAlertNotification(false);
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $provide) {

        $stateProvider
            .state('registration', {
                url: '/registration',
                templateUrl: 'app/registration/registration.html',
                controller: 'RegistrationController',
                controllerAs: '$ctrl'
            })
            .state('voting', {
                url: '/voting/:issue_id',
                templateUrl: 'app/voting/voting.html',
                controller: 'VotingController',
                controllerAs: '$ctrl'
            })
            .state('home', {
                url: '/home',
                templateUrl: 'app/home/home.html',
                controller: 'HomeController',
                controllerAs: '$ctrl'
            });


        $urlRouterProvider.otherwise('/registration');

        $provide.value('Narada', { host: null, deputyId: null, deviceId: null});

    });
