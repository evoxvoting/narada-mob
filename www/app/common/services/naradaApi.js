(function (angular) {
    'use strict';

    angular.module('app').factory('NaradaApi', factory);

    function factory($http, Narada) {

        function sendVote(issueId, vote) {
            return $http.post(Narada.host + '/voting/' + issueId + '/vote', {
                issue_id: issueId,
                deputy_id: Narada.deputyId,
                device_id: Narada.deviceId,
                vote: vote
            });
        }

        function register() {
            return $http.post(Narada.host + '/deputy/' + Narada.deputyId + '/register/' + Narada.deviceId);
        }

        function getIssue(issueId) {
            return $http.get(Narada.host + '/issue/' + issueId);
        }

        return {
            sendVote: sendVote,
            register: register,
            getIssue: getIssue
        };
    }
})(angular);
