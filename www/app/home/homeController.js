(function (angular) {
    'use strict';

    angular.module('app').controller('HomeController', controller);

    function controller($scope, $ionicHistory, $state) {

        $scope.$on('$ionicView.enter', refresh);
        $scope.$on('push-message', onNotification);


        function refresh() {
            $ionicHistory.clearHistory();
        }

        function onNotification(event, data) {
            $state.go('voting', {issue_id: data.additionalData.issue_id});
        }
    }
})(angular);