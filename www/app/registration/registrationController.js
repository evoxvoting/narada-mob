(function (angular) {
    'use strict';

    angular.module('app').controller('RegistrationController', controller);

    function controller($state, $cordovaBarcodeScanner, NaradaApi, Narada, $ionicPopup, $ionicLoading) {
        var vm = this;
        vm.register = register;

        function register() {
            $ionicLoading.show({template: 'Чекайте...'});
            window.plugins.OneSignal.getIds(function(ids) {
              Narada.deviceId = ids.userId;
            });
            $cordovaBarcodeScanner
                .scan()
                .then(function (barcodeData) {
                    var obj = JSON.parse(barcodeData.text);
                    Narada.host = obj.host;
                    Narada.deputyId = obj.deputyId;
                })
                .then(NaradaApi.register)
                .finally($ionicLoading.hide)
                .then($state.go.bind($state, 'home'))
                .catch(error);
        }

        function error(err){
            console.error(err);
            $ionicPopup.alert({title: 'Помилка', template: JSON.stringify(err)});
        }
    }
})(angular);
