(function (angular) {
    'use strict';

    angular.module('app').controller('VotingController', controller);

    function controller($scope, $ionicLoading, $state, NaradaApi) {
        var vm = this;
        vm.startVoting = startVoting;
        vm.stopVoting = stopVoting;
        vm.issue = {};
        var interval, vote;

        $scope.$on('$ionicView.enter', refresh);

        //////////////////////////////////////////////

        function refresh() {
            NaradaApi.getIssue($state.params.issue_id)
                .then(function(response){
                    vm.issue = response.data;
                })
                .catch(error);
        }

        function startInterval() {
            vm.time = 5;
            vm.interval = true;
            interval = setInterval(function () {
                $scope.$apply(function () {
                    --vm.time;
                    if (vm.time == 0 && vm.interval) {
                        vm.interval = false;
                        votingSuccess();
                    }
                });
                (!vm.time || !vm.interval) && clearInterval(interval);
            }, 1000);
        }

        function startVoting(selectedVote) {
            startInterval();
            vote = selectedVote;
        }

        function stopVoting() {
            vm.interval = false;
            vote = null;
            clearInterval(interval);
        }

        function votingSuccess() {
            var resultVote = vote;
            stopVoting();

            NaradaApi.sendVote(vm.issue._id, resultVote)
                .then(function () {
                    $state.go('home');
                });
        }

        function error(er) {
            $ionicPopup.alert({title: 'Помилка', template: JSON.stringify(err)});
        }
    }
})(angular);